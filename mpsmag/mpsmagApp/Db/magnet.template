#Diag Status power converter 
record(bi, "$(P):$(R):$(SYSTEM-ID)-QuadOK")
{ 
    field(DTYP, "OPCUA")
    field(INP,  "@$(SYSTEM-ID)::#$(SEG)Diag element=$(FIELD)")
    field(SCAN, "I/O Intr")
    field(TSE, "-2")
    field(OSV, "NO_ALARM")
    field(ONAM, "Ok")
    field(ZSV, "MAJOR")
    field(ZNAM, "Not ok")
    field(DESC, "$(P) Status")

    info(aa_policy, "default")
    info(archiver, "tn")
}

#Display Status Power converter
record(bi, "$(P):$(R):$(SYSTEM-ID)-DisplayStateOK")
{
    field(DTYP, "OPCUA")
    field(INP,  "@$(SYSTEM-ID)::#$(SEG)Display element=$(FIELD)")
    field(SCAN, "I/O Intr")
    field(TSE, "-2")
    field(ONAM, "Ok")
    field(ZNAM, "Not ok")
    field(DESC, "$(P) Display Status")

    info(aa_policy, "default")
    info(archiver, "tn")
}


#BP from Power Converter to MPSMag
record(bi, "$(P):$(R):$(SYSTEM-ID)-BPEn")
{
    field(DTYP, "OPCUA")
    field(INP,  "@$(P):$(R):#$(SYSTEM-ID)-Diag element=PwrCnvBP_Enbld")
    field(SCAN, "I/O Intr")
    field(TSE, "-2")
    field(ONAM, "Ok")
    field(ZNAM, "Not Ok")
    field(DESC, "PwrCnvBP is enabled")

    info(aa_policy, "default")
    info(archiver, "tn")
} 

#BP input channel status for Power Converter
record(bi, "$(P):$(R):$(SYSTEM-ID)-BPCHStat")
{
    field(DTYP, "OPCUA")
    field(INP,  "@$(P):$(R):#$(SYSTEM-ID)-Diag element=PwrCnvBP_CHStatus")
    field(SCAN, "I/O Intr")
    field(TSE, "-2")
    field(OSV, "NO_ALARM")
    field(ONAM, "Valid")
    field(ZSV, "MINOR")
    field(ZNAM, "Failsafe/Deactivated")
    field(DESC, "PwrCnvBP Input CH status")

    info(aa_policy, "default")
    info(archiver, "tn")
} 

#BP discrepancy error for Power Converter
record(bi, "$(P):$(R):$(SYSTEM-ID)-BPErr")
{
    field(DTYP, "OPCUA")
    field(INP,  "@$(P):$(R):#$(SYSTEM-ID)-Diag element=PwrCnvBP_Discrep_Error")
    field(SCAN, "I/O Intr")
    field(TSE, "-2")
    field(OSV, "MAJOR")
    field(ONAM, "Discrepancy Error")
    field(ZSV, "NO_ALARM")
    field(DESC, "PwrCnvBP Discr.Err.")

    info(aa_policy, "default")
    info(archiver, "tn")
} 

#BP cable to power converter
record(bi, "$(P):$(R):$(SYSTEM-ID)-BPCblCnnctEn")
{
    field(DTYP, "OPCUA")
    field(INP,  "@$(P):$(R):#$(SYSTEM-ID)-Diag element=PwrCnvBP_CblCnnct_Enbld")
    field(SCAN, "I/O Intr")
    field(TSE, "-2")
    field(OSV, "NO_ALARM")
    field(ONAM, "Connected")
    field(ZSV, "MINOR")
    field(ZNAM, "Not Connected")
    field(DESC, "PwrCnvBP Cable to pwr.cnv.cnnct")

    info(aa_policy, "default")
    info(archiver, "tn")
} 

#BP cable to power converter status
record(bi, "$(P):$(R):$(SYSTEM-ID)-BPCblCnnctCHStat")
{
    field(DTYP, "OPCUA")
    field(INP,  "@$(P):$(R):#$(SYSTEM-ID)-Diag element=PwrCnvBP_CblCnnct_CHStatus")
    field(SCAN, "I/O Intr")
    field(TSE, "-2")
    field(OSV, "NO_ALARM")
    field(ONAM, "Valid")
    field(ZSV, "MINOR")
    field(ZNAM, "Failsafe/Deactivated")
    field(DESC, "PwrCnv Cable connct.CH stat")

    info(aa_policy, "default")
    info(archiver, "tn")
} 

#Power Converter Masked 
record(bi, "$(P):$(R):$(SYSTEM-ID)-FilterEn")
{
    field(DTYP, "OPCUA")
    field(INP,  "@$(P):$(R):#$(SYSTEM-ID)-Diag element=Filtering_Enbld")
    field(SCAN, "I/O Intr")
    field(TSE, "-2")
    field(OSV, "MINOR")
    field(ONAM, "true")
    field(ZSV, "NO_ALARM")
    field(ZNAM, "false")
    field(DESC, "PwrCnv is Filtered")

    info(aa_policy, "default")
    info(archiver, "tn")
} 

#Power Converter feeds magnet Downstream Beam Destination 
record(bi, "$(P):$(R):$(SYSTEM-ID)-DwnStrmBDEn")
{
    field(DTYP, "OPCUA")
    field(INP,  "@$(P):$(R):#$(SYSTEM-ID)-Diag element=DownStream_BDest_Enbld")
    field(SCAN, "I/O Intr")
    field(TSE, "-2")
    field(ONAM, "Magnet Downstream BD")
    field(DESC, "PwrCnv is Downstream BD")

    info(aa_policy, "default")
    info(archiver, "tn")
} 

#Summary Discrepancy Error in Power Converter 
record(bi, "$(P):$(R):$(SYSTEM-ID)-SumErr")
{
    field(DTYP, "OPCUA")
    field(INP,  "@$(P):$(R):#$(SYSTEM-ID)-Diag element=Sum_Err")
    field(SCAN, "I/O Intr")
    field(TSE, "-2")
    field(OSV, "MAJOR")
    field(ONAM, "Sum.Discr.Err.PwrCnv")
    field(ZSV, "NO_ALARM")
    field(DESC, "Summ.Err. any Discr.PwrCnv")

    info(aa_policy, "default")
    info(archiver, "tn")
}

#internal record to hide summ error icon if filter is enabled.
record(calcout, "$(P):$(R):#$(SYSTEM-ID)-SumErrIcon")
{
    field(INPA, "$(P):$(R):$(SYSTEM-ID)-SumErr CP") # Summary error
    field(INPB, "$(P):$(R):$(SYSTEM-ID)-FilterEn CP") # Valve filtered
    field(OUT, "$(P):$(R):$(SYSTEM-ID)-SumErrIcon.PROC") # output to bi record
    field(CALC, "A&&!B") # TRUE (=1) if there is Summary error and the valve is not filtered
    field(DESC, "Summary Error Icon")
}

#record to show error icon to OPI.
record(bi, "$(P):$(R):$(SYSTEM-ID)-SumErrIcon")
{
    field(INP, "$(P):$(R):#$(SYSTEM-ID)-SumErrIcon") # difference in MaskEn
    field(OSV, "MINOR")
    field(ONAM, "True")
    field(ZSV, "NO_ALARM")
    field(ZNAM, "False")
    field(DESC, "Summary eEror icon to OPI")
}

#Power converter BP masked
record(bi, "$(P):$(R):$(SYSTEM-ID)-BPMasked")
{
    field(DTYP, "OPCUA")
    field(INP,  "@$(P):$(R):#$(SYSTEM-ID)-Diag element=PwrCnvBP_Masked")
    field(SCAN, "I/O Intr")
    field(TSE, "-2")
    field(OSV, "MINOR")
    field(ONAM, "True")
    field(ZSV, "NO_ALARM")
    field(ZNAM, "False")
    field(DESC, "Power converter beam permit is Masked")

    info(aa_policy, "default")
    info(archiver, "tn")
}

#Magnet any mask enabled
record(bi, "$(P):$(R):$(SYSTEM-ID)-AnyMaskEn")
{
    field(DTYP, "OPCUA")
    field(INP,  "@$(P):$(R):#$(SYSTEM-ID)-Diag element=Any_Mask_Enbld")
    field(SCAN, "I/O Intr")
    field(TSE, "-2")
    field(OSV, "MINOR")
    field(ONAM, "True")
    field(ZSV, "NO_ALARM")
    field(ZNAM, "False")
    field(DESC, "One or more input are Masked")

    info(aa_policy, "default")
    info(archiver, "tn")
}

#internal record to detect difference from any table input.
record(calcout, "$(P):$(R):#$(SYSTEM-ID)-MaskCfg-UM")
{
    field(INPA, "$(P):$(R):$(SYSTEM-ID)-MaskEn-UM CP") # difference in MaskEn
    field(INPB, "$(P):$(R):$(SYSTEM-ID)-MaskBP-UM CP") # difference in MaskBP
    field(OUT, "$(P):$(R):$(SYSTEM-ID)-MaskCfg-UM.PROC") # output to bi record
    field(CALC, "A||B") # TRUE (=1) if there is any input unmatched
    field(DESC, "Masking configuration unmatching")
}

#record to show the unmatching of mask to OPI.
record(bi, "$(P):$(R):$(SYSTEM-ID)-MaskCfg-UM")
{
    field(INP, "$(P):$(R):#$(SYSTEM-ID)-MaskCfg-UM") # difference in MaskEn
    field(OSV, "MINOR")
    field(ONAM, "True")
    field(ZSV, "NO_ALARM")
    field(ZNAM, "False")
    field(DESC, "Masking configuration unmatching to OPI")
}
